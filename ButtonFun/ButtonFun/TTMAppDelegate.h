//
//  TTMAppDelegate.h
//  ButtonFun
//
//  Created by admin on 9/8/14.
//  Copyright (c) 2014 TriThucMoi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTMViewController;
@interface TTMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) TTMViewController *viewController;

@end
