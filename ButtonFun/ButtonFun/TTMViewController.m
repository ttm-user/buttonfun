//
//  TTMViewController.m
//  ButtonFun
//
//  Created by admin on 9/8/14.
//  Copyright (c) 2014 TriThucMoi. All rights reserved.
//

#import "TTMViewController.h"

@interface TTMViewController ()

@end

@implementation TTMViewController
@synthesize statusText;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPressed:(UIButton *)sender {
    NSString *title = [sender titleForState:UIControlStateNormal];
    statusText.text = [NSString stringWithFormat:@"%@ button pressed.", title];
}
@end
