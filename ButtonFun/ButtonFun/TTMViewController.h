//
//  TTMViewController.h
//  ButtonFun
//
//  Created by admin on 9/8/14.
//  Copyright (c) 2014 TriThucMoi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTMViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *statusText;
- (IBAction)buttonPressed:(UIButton *)sender;

@end
